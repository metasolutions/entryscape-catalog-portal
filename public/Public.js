define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dijit/_WidgetBase",
    "entryscape-commons/defaults",
    "entryscape-catalog-portal/dataset/Dataset",
    "entryscape-commons/rdforms/linkBehaviour"
], function (declare, lang, domConstruct, _WidgetBase, defaults, Dataset, linkBehaviour) {

    var entrystoreutil = defaults.get("entrystoreutil");

    return declare([_WidgetBase], {
        buildRendering: function () {
            this.domNode = this.srcNodeRef || domConstruct.create("div");
            this.viewNode = domConstruct.create("div", null, this.domNode);
        },

        show: function(params) {
            var store = defaults.get("entrystore");
            var context = defaults.get("context");
            if (context && params.entry) {
                this.entryPromise = store.getEntry(store.getEntryURI(context.getId(), params.entry));
                this.entryPromise.then(lang.hitch(this, this.showEntry));
            }
            if (params.resource) {
                this.entryPromise = entrystoreutil.getEntryByResourceURI(params.resource, context);
                this.entryPromise.then(lang.hitch(this, this.showEntry));
            }
        },

        showEntry: function(entry) {
            if (this.viewer) {
                this.viewer.destroy();
                delete this.viewer;
            }
            this.viewer = new Dataset({inDialog: false}, domConstruct.create("div", null, this.viewNode));
            this.viewer.startup();
            this.viewer.showEntry(entry);
        }
    });
});