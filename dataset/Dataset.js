define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/Deferred",
    "dojo/on",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/promise/all",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "di18n/NLSMixin",
    "rdforms/model/system",
    "entryscape-commons/defaults",
    "config",
    "store/solr",
    "rdforms/view/Presenter", //In template
    "entryscape-commons/rdforms/RDFormsPresentDialog", //In template
    "entryscape-catalog-portal/dataset/DistributionDialog", //In template
    "entryscape-commons/rdforms/linkBehaviour",
    "dojo/text!./DatasetTemplate.html",
    "rdforms/view/bootstrap/all" //Indirect in template
], function (declare, lang, array, Deferred, on, domAttr, domConstruct, domClass, domStyle, all, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin, system, defaults, config, solr, Presenter, RDFormsPresentDialog, DistributionDialog, linkBehaviour, template) {

    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, NLSMixin.Dijit], {
        templateString: template,
        inDialog: false,
        nlsBundles: ["catalogpublic"],

        postCreate: function() {
            this.inherited("postCreate", arguments);

            if (!this.inDialog) {
                domClass.add(this.domNode, "mainView");
                domConstruct.place(this.detailsBlock, this.details);
                domConstruct.place(this.catalogBlock, this.sidebar);
                domConstruct.place(this.distributionBlock, this.sidebar);
                domConstruct.place(this.resultBlock, this.sidebar);
            }
        },
        show: function(params) {
            var store = defaults.get("entrystore");
            var context = defaults.get("context");
            var entryId = params.entry;
            if (context && entryId) {
                this.entryPromise = store.getEntry(store.getEntryURI(context.getId(), entryId));
                this.entryPromise.then(lang.hitch(this, this.showDataset));
            }
        },
        showEntry: function(entry, params) {
            this.entryPromise = new Deferred();
            this.entryPromise.resolve(entry);
            this.showDataset(entry);
        },

        localeChange: function() {
            if (this.entryPromise) {
                this.entryPromise.then(lang.hitch(this, this.showDataset));
            }
        },

        showDataset: function(entry) {
            defaults.get("itemstore", lang.hitch(this, function(itemstore) {
                this.entry = entry;
                var rdfutils = defaults.get("rdfutils"),
                    template = itemstore.getItem(config.catalog.datasetTemplateId);
                this.presenter.show({resource: entry.getResourceURI(), graph: entry.getMetadata(), template: template});

                this.fetchCatalog(entry).then(lang.hitch(this, function(catalogEntry) {
                    domAttr.set(this.catalog, "innerHTML", rdfutils.getLabel(catalogEntry));
                    this.dcatEntry = catalogEntry;
                }));

                this.fetchDistributions(entry).then(lang.hitch(this, this.showDistributions));
                this.fetchResults(entry).then(lang.hitch(this, this.showResults));
            }));
        },

        fetchCatalog: function(datasetEntry) {
            var store = defaults.get("entrystore");
            return store.createSearchList(solr.rdfType("dcat:Catalog").limit(2).context(
                datasetEntry.getContext())).getEntries(0).then(function(entryArr) {
                if (entryArr.length !== 1) {
                    throw "Wrong number of entries.";
                }
                return entryArr[0];
            });
        },

        fetchDistributions: function(datasetEntry) {
            var storeutil = defaults.get("entrystoreutil");
            var md = datasetEntry.getMetadata(),
                stmts = md.find(datasetEntry.getResourceURI(), "dcat:distribution");
            return all(array.map(stmts, function(stmt) {
                return storeutil.getEntryByResourceURI(stmt.getValue()).then(function(entry) {
                    return entry;
                }, function(error) {
                    //fail silently for missing distributions, list those that do exist.
                    return null;
                });
            }));
        },

        fetchResults: function(datasetEntry) {
            var list = [];
            return defaults.get("entrystore").newSolrQuery().rdfType("esterms:Result")
                .context(datasetEntry.getContext())
                .uriProperty("dcterms:source", datasetEntry.getResourceURI())
                .list().forEach(function(result) {
                list.push(result);
            }).then(function() {
                return list;
            });
        },

        showDistributions: function(dists) {
            var rdfutils = defaults.get("rdfutils");
                domAttr.set(this.distributions, "innerHTML", "");
            array.forEach(dists, function(distE) {
                if (distE != null) {
                    var md = distE.getMetadata();
                    var subj = distE.getResourceURI();
                    var title = md.findFirstValue(subj, "http://purl.org/dc/terms/title");
                    var desc = md.findFirstValue(subj, "http://purl.org/dc/terms/description");
                    var access = md.findFirstValue(subj, "http://www.w3.org/ns/dcat#accessURL");
                    var label = title || desc || access;
                    var tr = domConstruct.create("tr", null, this.distributions),
                        tdLabel = domConstruct.create("td", {"innerHTML": label}, tr),
                        tdButtons = domConstruct.create("td", null, tr),
                        infoButton = domConstruct.create("button", {"class": "btn btn-sm btn-default"}, tdButtons),
                        icon = domConstruct.create("span", {"class": "fa fa-info-circle"}, infoButton);
                    var f = lang.hitch(this, function(ev) {
                        ev.stopPropagation();
                        var rdfutils = defaults.get("rdfutils");
                        this.distributionInfoDialog.set("title", label);

                        this.distributionInfoDialog.open(distE);
                    });
                    on(infoButton, "click", f);
                    on(tr, "click", f);
                }
            }, this);
        },

        showResults: function(results) {
            if (results.length === 0) {
                domStyle.set(this.resultBlock, "display", "none");
                return;
            }
            domStyle.set(this.resultBlock, "display", "");
            var rdfutils = defaults.get("rdfutils");
            domAttr.set(this.results, "innerHTML", "");
            array.forEach(results, function(resultE) {
                if (resultE != null) {
                    var md = resultE.getMetadata();
                    var subj = resultE.getResourceURI();
                    var title = md.findFirstValue(subj, "dcterms:title");
                    var desc = md.findFirstValue(subj, "dcterms:description");
                    var access = md.findFirstValue(subj, "foaf:page");
                    var label = title || desc || access;
                    var tr = domConstruct.create("tr", null, this.results),
                        tdLabel = domConstruct.create("td", {"innerHTML": label}, tr),
                        tdButtons = domConstruct.create("td", null, tr),
                        infoButton = domConstruct.create("button", {"class": "btn btn-sm btn-default"}, tdButtons),
                        icon = domConstruct.create("span", {"class": "fa fa-info-circle"}, infoButton);
                    var f = lang.hitch(this, function(ev) {
                        ev.stopPropagation();
                        var rdfutils = defaults.get("rdfutils");
                        this.infoDialog.title = label;
                        this.infoDialog.localeChange();
                        var template = defaults.get("itemstore").getItem(config.catalog.datasetResultTemplateId);
                        this.infoDialog.show(resultE.getResourceURI(), resultE.getMetadata(), template);
                    });
                    on(infoButton, "click", f);
                    on(tr, "click", f);
                }
            }, this);
        },

        onCatalogClick: function(ev) {
            ev.stopPropagation();
            var template = defaults.get("itemstore").getItem(config.catalog.catalogTemplateId);
            var rdfutils = defaults.get("rdfutils");
            this.infoDialog.title = rdfutils.getLabel(this.dcatEntry);
            this.infoDialog.localeChange();
            this.infoDialog.show(this.dcatEntry.getResourceURI(), this.dcatEntry.getMetadata(), template);
        }
    });
});