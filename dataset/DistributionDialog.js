define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "entryscape-commons/defaults",
    "rdforms/view/Presenter", //In template
    "entryscape-commons/dialog/HeaderDialog", //In template
    "entryscape-catalog/files/APIInfo", //In template
    "entryscape-commons/rdforms/RDFormsPresentDialog",
    "config",
    "dojo/text!./DistributionDialogTemplate.html",
    "rdforms/view/bootstrap/all"
], function (declare, lang, defaults, Presenter, HeaderDialog, APIInfo, RDFormsPresentDialog, config, template) {

    return declare([RDFormsPresentDialog], {
        templateString: template,
        checkForAPI: true,

        open: function(entry) {
            if (this.checkForAPI) {
                this.updateApiInfo(entry);
            }
            this.show(entry.getResourceURI(), entry.getMetadata(),
                defaults.get("itemstore").getItem(config.catalog.distributionTemplateId));
        },
        updateApiInfo: function(entry) {
            this.apiInfo.hide();
            var md = entry.getMetadata(),
                es = defaults.get("entrystore"),
                ns = defaults.get("namespaces");
            var fileEntryResourceURI = md.findFirstValue(entry.getResourceURI(), ns.expand("dcterms:source"));
            if (fileEntryResourceURI != null && fileEntryResourceURI.indexOf(es.getBaseURI()) === 0) {
                var fileEntryURI = es.getEntryURI(es.getContextId(fileEntryResourceURI), es.getEntryId(fileEntryResourceURI));
                es.getEntry(fileEntryURI).then(lang.hitch(this, function(fileEntry) {
                    var pipelineResults = fileEntry.getReferrers(ns.expand("store:pipelineData"));
                    if (pipelineResults.length > 0) {
                        es.getEntry(pipelineResults[0]).then(lang.hitch(this, function(pipelineResult) {
                            this.apiInfo.show(pipelineResult);
                        }));
                    }
                }));
            }
        }
    });
});