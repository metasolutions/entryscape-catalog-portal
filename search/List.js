define([
    "dojo/_base/declare",
    "entryscape-commons/defaults",
    "entryscape-catalog-portal/dataset/DatasetDialog",
    "entryscape-commons/list/common/BaseList",
    "entryscape-catalog/utils/ListView"
], function (declare, defaults, DatasetDialog, BaseList, ListView) {

    var ns = defaults.get("namespaces");

    return declare([BaseList], {
        includeCreateButton: false,
        includeInfoButton: true,
        includeEditButton: false,
        includeRemoveButton: false,
        includeHead: true,
        searchVisibleFromStart: true,
        nlsBundles: ["list", "dataset"],
        specificNLSBundleName: "dataset",
        entryType: ns.expand("dcat:Dataset"),
        listViewClass: ListView,
        "class": "datasets",
        rowClickDialog: "info",

        postCreate: function () {
            this.inherited("postCreate", arguments);
            this.registerDialog("info", DatasetDialog)
        },
        showStopSign: function() {
            return false;
        },
        installButtonOrNot: function(params, row) {
            return true;
        },

        getTemplate: function (entry) {
            if (!this.template) {
                this.template = defaults.get("itemstore").getItem("dcat:OnlyDataset");
            }
            return this.template;
        },
        getSearchObject: function() {
            var so = defaults.get("entrystore").newSolrQuery().rdfType(this.entryType);
            var context = defaults.get("context");
            if (context) {
                so.context(context);
            }
            return so;
        }
    });
});