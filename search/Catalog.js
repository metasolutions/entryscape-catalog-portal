define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom-construct",
    "dojo/dom-attr",
    "dojo/dom-class",
    "dojo/_base/array",
    "config",
    "entryscape-commons/defaults",
    "entryscape-commons/rdforms/RDFormsPresentDialog",
    "./List",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "di18n/NLSMixin",
    "di18n/localize",
    "jquery",
    "dojo/text!./CatalogTemplate.html"
], function(declare, lang, domConstruct, domAttr, domClass, array, config, defaults, RDFormsPresentDialog, List, _WidgetBase, _TemplatedMixin, NLSMixin, localize, jquery, template) {


    return declare([_WidgetBase, _TemplatedMixin, NLSMixin.Dijit], {
        bid: "espoCatalog",
        templateString: template,
        nlsBundles: ["espoCatalog"],

        __catalogList: null,
        __main: null,
        __label: null,
        __description: null,
        __back: null,

        postCreate: function() {
            this.inherited(arguments);
            this.list = new List({}, domConstruct.create("div", null, this.__main));
            this.list.render();
        },

        show: function (viewParams) {
            this.inherited(arguments);
            if (this.catalogs) {
                this.render(this.catalogs);
            } else {
                var rdfutils = defaults.get("rdfutils");
                var catalogs = [];
                var query = defaults.get("entrystore").newSolrQuery().rdfType("dcat:Catalog");
                if (config.catalog && config.catalog.excludeEmptyCatalogsInSearch) {
                    query = query.uriProperty("dcat:dataset", "*");
                }
                query.list()
                    .forEach(function(catalogEntry) {
                        catalogs.push({
                            entry: catalogEntry,
                            label: rdfutils.getLabel(catalogEntry),
                            nr: catalogEntry.getMetadata().find(catalogEntry.getResourceURI(), "dcat:dataset").length
                        });
                    }).then(lang.hitch(this, function() {
                    catalogs.sort(function(c1, c2) {
                        return c1.nr < c2.nr ? 1 : -1;
                    });
                    this.render(catalogs);
                }));
            }
        },

        render: function(catalogs) {
            this.catalogs = catalogs;
            var context = defaults.get("context");
            domAttr.set(this.__catalogList, "innerHTML", "");

            var catalog;
            if (catalogs.length === 1) {
                catalog = catalogs[0];

            } else if (context) {
                array.some(catalogs, function(c) {
                    if (c.entry.getContext() === context) {
                        catalog = c;
                        return true;
                    }
                });
            }

            var site = defaults.get("siteManager");
            var params = site.getCurrentOrUpcomingParams();
            var view = site.getCurrentOrUpcomingView();
            if (catalog) {
                this.catalogEntry = catalog.entry;
                var rdfutils = defaults.get("rdfutils");
                domClass.remove(this.domNode, this.bid+"--list");
                domAttr.set(this.__label, "innerHTML", catalog.label);
                domAttr.set(this.__description, "innerHTML", rdfutils.getDescription(catalog.entry));
                var p = lang.clone(params);
                delete p.context;
                if (catalogs.length <= 1) {
                    domClass.add(this.domNode, this.bid+"--noListAvailable");
                } else {
                    domClass.remove(this.domNode, this.bid+"--noListAvailable");
                    domAttr.set(this.__back, "href", site.getHashUrl(view, p));
                }
            } else {
                domClass.add(this.domNode, this.bid+"--list");
                array.forEach(catalogs, function(catalog) {
                    var p = lang.clone(params);
                    p.context = catalog.entry.getContext().getId();
                    domConstruct.create("a", {
                        "class": "list-group-item",
                        href: site.getHashUrl(view, p),
                        innerHTML: "<span class='badge'>" + catalog.nr + "</span>"+ catalog.label
                    }, this.__catalogList);
                }, this);
            }
            this.list.getView().action_refresh();
        },
        infoCatalogClick: function() {
            if (!this.rdfdialog) {
                this.rdfdialog = new RDFormsPresentDialog({maxWidth: 800});
            }
            var template = defaults.get("itemstore").getItem(config.catalog.catalogTemplateId);
            var resourceURI = this.catalogEntry.getResourceURI();
            var md = this.catalogEntry.getMetadata();
            this.rdfdialog.show(resourceURI, md, template);
        }
    });
});