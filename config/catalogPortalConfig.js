define({
    site: {
        views: [
            {
                "name": "datasetsearch",
                "class": "entryscape-catalog-portal/search/List",
                "title": {en: "Search", sv: "Sök"}
            },
            {
                "name": "catalogsearch",
                "class": "entryscape-catalog-portal/search/Catalog",
                "title": {en: "Search datasets", sv: "Sök datamängder"}
            },
            {
                "name": "public",
                "class": "entryscape-catalog-portal/public/Public",
                "title": {en: "Dataset", sv: "Datamängd"}
            }
        ]
    }
});