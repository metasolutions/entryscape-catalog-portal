define([
    "entryscape-commons/merge",
    "./catalogPortalConfig",
    "entryscape-catalog/config/catalogConfig"
], function(merge, catalogPortalConfig, catalogConfig) {
        return merge(catalogConfig, catalogPortalConfig, {
            theme: {
                appName: "EntryScape Catalog",
                oneRowNavbar: true
            },
            locale: {
                fallback: "en",
                supported: [
                    {lang: "en", flag: "gb", label: "English", labelEn: "English"},
                    {lang: "sv", flag: "se", label: "Svenska", labelEn: "Swedish"}
                ]
            },
            catalog: {
                excludeEmptyCatalogsInSearch: true
            },
            site: {
                noLogin: true,
                siteClass: "spa/Site",
                controlClass: "entryscape-commons/nav/Layout",
                startView: "catalogsearch"
            }
        }, __entryscape_config);
});