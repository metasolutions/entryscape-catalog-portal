#!/bin/bash

rm -rf ../merged-nls/*
cp -a ../libs/entryscape-commons/nls/* ../merged-nls
cp -a ../libs/entryscape-catalog/nls/* ../merged-nls
cp -a ../nls/* ../merged-nls