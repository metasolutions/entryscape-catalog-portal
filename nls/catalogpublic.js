define({
    "root": {
        datasetBelongsToCatalog: "The dataset belongs to a catalog",
        accessDistributionsOfDataset: "You can access the dataset via its distributions",
        datasetDetails: "Details of the dataset",
        resultsOfDataset: "Examples where the dataset have been used"
    },
    "sv": true
});
