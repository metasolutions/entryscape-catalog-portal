define({
    datasetBelongsToCatalog: "Datamängden hör till följande katalog",
    accessDistributionsOfDataset: "Du kan komma åt datamängden via dess distributioner",
    datasetDetails: "Information om datamängden",
    resultsOfDataset: "Exampel där datamängden använts"
});
